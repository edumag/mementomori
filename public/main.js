// Date in form datepicker
var picker = new Pikaday({
    field: document.getElementById('datepicker'),
    format: 'D/M/YYYY',
    toString(date, format) {
        // you should do formatting based on the passed format,
        // but we will just return 'D/M/YYYY' for simplicity
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return `${day}/${month}/${year}`;
    },
    parse(dateString, format) {
        // dateString is the result of `toString` method
        const parts = dateString.split('/');
        const day = parseInt(parts[0], 10);
        const month = parseInt(parts[1], 10) - 1;
        const year = parseInt(parts[2], 10);
        return new Date(year, month, day);
    },
    i18n: {
        previousMonth: 'Previous Month',
        nextMonth: 'Next Month',
        months: ['Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembrer', 'Octubre', 'Nbembre', 'Decembre'],
        weekdays: ['Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte'],
        weekdaysShort: ['Diu', 'Dill', 'Dim', 'Dic', 'Dij', 'Div', 'Dis']
    }
});

// Date in form datepicker_mort.
var picker_mort = new Pikaday({
    field: document.getElementById('datepicker_mort'),
    format: 'D/M/YYYY',
    toString(date, format) {
        // you should do formatting based on the passed format,
        // but we will just return 'D/M/YYYY' for simplicity
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return `${day}/${month}/${year}`;
    },
    parse(dateString, format) {
        // dateString is the result of `toString` method
        const parts = dateString.split('/');
        const day = parseInt(parts[0], 10);
        const month = parseInt(parts[1], 10) - 1;
        const year = parseInt(parts[2], 10);
        return new Date(year, month, day);
    },
    i18n: {
        previousMonth: 'Previous Month',
        nextMonth: 'Next Month',
        months: ['Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembrer', 'Octubre', 'Nbembre', 'Decembre'],
        weekdays: ['Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte'],
        weekdaysShort: ['Diu', 'Dill', 'Dim', 'Dic', 'Dij', 'Div', 'Dis']
    }
});

// sessionStorage.removeItem('morts');


// if (window.sessionStorage) {
// 
//     if ( sessionStorage.getItem('morts') ) {// 
//        const morts = sessionStorage.getItem('morts');
//        // console.log({morts: morts})
//     } else {// 
//         sessionStorage.setItem('morts', []);
//     }
// }
// else {
//    throw new Error('Tu Browser no soporta sessionStorage!');
// }

function submit_form() {

    let morts = JSON.parse(localStorage.getItem('morts'));    

    if ( ! morts ) {
        morts = [];
    }

    console.log({morts: morts});

    const form = document.getElementById('form_mort');

    mort = {
        
        names: form['names'].value,
        femeni: form['femeni'][0].checked,
        data_neixament: form['data_neixament'].value,
        causa: form['causa'].value,
        data_mort: form['data_mort'].value,
        familiars: form['familiars'].value

    }

    // socket.emit('printer', mort);


   location.href = './tumba.html';
}
