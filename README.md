# MememtoMori

## Añadir datos de ejemplo

En servidor:

https://mementomori.lesolivex.com/datatest

En local:

http://localhost:5000/datatest

## URLs

- /pendent

Devuelve json con los registros pendientes de imprimir y los marca para saber que ya están imprimidos.

- /print

Llama a /pendent del servidor, ejecuta los ficheros en formato latex pendientes.
 
## PM2

https://www.digitalocean.com/community/tutorials/como-configurar-una-aplicacion-de-node-js-para-produccion-en-ubuntu-18-04-es

Instalación de la aplicación en el sistema para que se ejecute en el inicio.

## Cron

ejecutar:

```
sudo crontab -e
```

Añadir al final.

```
* * * * * /home/edu/desarrollo/mementomori/PDFs/print_cron.sh
```

Cron debe ejecutar cada minuto el script PDFs/print_cron.sh.

En caso de necesitar hacerlo en segundos aquí tenemos un truco.

https://es.linux-console.net/?p=2777#gsc.tab=0

## Impresora

Las esquelas generadas serán enviadas a la impresora por defecto.

Para hacer pruebas se puede instalar cups-pdf para simular una impresora.

## Logs

### Aplicación

```
pm2 log index
```

### Registros de print_cron.sh

```
tail -f /var/log/syslog
```

