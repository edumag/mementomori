const express = require('express')
const app = express()
const server = require("http").Server(app);
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();
const body_parser = require('body-parser');

const port = 5000
const path = require('path')

const axios = require("axios");

const datatest = [
{
  id: 26,
  names: 'Eduard Magrané Sánchez',
  causa: 'simplemente murio corriendo',
  familiars: 'Ninguno se intereso',
  data_neixament: '21/2/2023',
  data_mort: '21/2/2023'
},
{
  id: 25,
  names: 'Lorem Ipsum es simplemente',
  causa: 'Lorem Ipsum es simplemente el texto de rellen',
  familiars: 'Lorem Ipsum es simplemente el texto de relleno de ',
  data_neixament: '21/2/2023',
  data_mort: '21/2/2023'
},
{
  id: 24,
  names: 'Lorem Ipsum es simplemente II',
  causa: 'Lorem Ipsum es simplemente el texto de relleno',
  familiars: 'Lorem Ipsum es simplemente el texto de relleno de ',
  data_neixament: '14/2/2023',
  data_mort: '21/2/2023'
},
{
  id: 23,
  names: 'Lorem Ipsum es simplemente III',
  causa: 'Lorem Ipsum es simplemente',
  familiars: 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de te',
  data_neixament: '14/2/2023',
  data_mort: '20/2/2023'
},
{
  id: 22,
  names: 'Lorem Ipsum es simplemente IV',
  causa: 'Lorem Ipsum es simplemente el texto de relleno',
  familiars: 'Lorem Ipsum es simplemente',
  data_neixament: '15/2/2023',
  data_mort: '20/2/2023'
},
{
  id: 21,
  names: 'Lorem Ipsum es simplemente V',
  causa: 'Hecho establecido hace demasiado tiempo que u',
  familiars: 'Al contrario del pensamiento popular, el texto de ',
  data_neixament: '21/2/2023',
  data_mort: '21/2/2023'
},
{
  id: 20,
  names: 'Lorem Ipsum es simplemente VI',
  causa: 'Es un hecho establecido hace demasiado tiempo',
  familiars: 'Al contrario del pensamiento popular, el texto',
  data_neixament: '21/2/2023',
  data_mort: '21/2/2023'
},
{
  id: 19,
  names: 'Lorem Ipsum es simplemente VII',
  causa: 'Es un hecho establecido hace demasiado',
  familiars: 'Al contrario del pensamiento popular, el texto de Lorem',
  data_neixament: '21/2/2023',
  data_mort: '21/2/2023'
},
{
  id: 18,
  names: 'Lorem Ipsum es simplemente VIII',
  causa: 'Es un hecho establecido hace',
  familiars: 'Al contrario del pensamiento popular, el texto de Lorem',
  data_neixament: '21/2/2023',
  data_mort: '21/2/2023'
}
];


app.use(body_parser.urlencoded({extended:true}));

app.use(express.static("public"));
app.set("view engine", "ejs");
app.set('views', path.join(__dirname, 'views')); 

app.get('/tumba', function (req, res) {
  console.log('ruta tumba');
  res.render('tumba', {data: rowsdb});  
});

app.get('/data', function (req, res) {
  console.log('ruta data');
  res.send(rowsdb);  
});

app.get("/pendent", (req, res) => {
  let sql = "SELECT id, names, causa, familiars, data_neixament, data_mort FROM mementomori WHERE mementomori.print != 1 ORDER BY id desc LIMIT 10";
  db.all(sql, [], (err, rows) => {
    // if (err) console.error(err);
    if (err) {
      res.status(400).json({"error":err.message});
      return;
    }

    db.run(`UPDATE mementomori set print = 1`,
      [],
      function (err, result) {
        if (err) {
          res.status(400).json({ "error": res.message })
          return;
        }
        console.log('Update all rows');
      });

      res.status(200).json(rows);

  });
});

app.get("/print", async (req, res) => {
  let url = 'https://mementomori.lesolivex.com/pendent';
  let salida = '<h1>Esquelas impresas</h1>';
  let data = null;
  let counter = 1;

  const response = await axios.get(url)
  console.log({RESPONSE: response.data});
  data = response.data;
  for (let e in data) {
    salida = salida + `<br/>Imprimiendo: ${data[e].id}: ${data[e].names}`;
    render_template(data[e], counter);
    counter++;
  }
  res.send(salida);  
})

app.post('/add', function (req, res) {

  console.log({body: req.body});
  save_data(req.body);
  // render_template(req.body);
  res.redirect('/submit.html');

});

app.get('/datatest', function (req, res) {

  for (let e in datatest) {
    console.log({datatest: datatest[e]});
    save_data(datatest[e]);
  }

  res.send('Insert datatest');  
});


//middleware que nos dirá qué ocurre en cada petición
app.use(function(req, res, next){
 
 //registra cada petición en la consola
 console.log(req.method, req.url);
 
 //continúamos haciendo lo que sea que estábamos haciendo y vamos a la ruta
 next();
});

let db = new sqlite3.Database('./sqlite.db', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to database.');
});

// Data of database.
var rowsdb = [];

// Read the port data

server.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

function save_data(data) {
  const stmt = db.prepare("INSERT INTO mementomori VALUES (?,?,?,?,?,?,?)");
  stmt.run(null, data.names, data.causa, data.familiars, data.data_neixament, data.data_mort, 0);
  stmt.finalize();
  console.log({'save in db': data});
  db_data();
}

function render_template(data, counter) {

  let outtex = 'PDFs/' + Date.now() + '-' + counter + '.tex';
  let senyorsenyora = 'El senyor';

  if ( data.femeni ) {

    senyorsenyora = 'La senyora';

  }

  try {
    let text = fs.readFileSync('public/dev/template.tex', 'utf8');
    text = text.replace('SENYORSENYORA', senyorsenyora);
    text = text.replace('NOMCOGNOMS', data.names);
    text = text.replace('CAUSADEMORT', data.causa);
    text = text.replace('NOMNAME', data.names);
    text = text.replace('FAMILIARS', data.familiars);
    text = text.replace('DATA1', data.data_neixament);
    text = text.replace('DATA2', data.data_mort);
    console.log(text);

    fs.writeFile(outtex, text, err => {
      if (err) {
        console.error(err);
      }
      console.log('File [' + outtex + '] generate.');
    });

  } catch (err) {
    console.error(err);
  }
}

// async function db_print_rows() {
// 
//   let sql = 'SELECT id, names, causa, familiars, data_neixament, data_mort FROM mementomori WHERE \'print\' != 1 ORDER BY id desc LIMIT 10';
// 
//   await db.all(sql, [], (err, rows) => {
//     let printsdb = [];
//     if (err) {
//       console.log(err);
//     }
//     rows.forEach((row) => {
//       printsdb.push(row);
//       console.log({pendiente: row.names});
//     });
//     return printsdb;
//   });
// 
// 
// }

function db_data() {

  let sql = 'SELECT id, names, causa, familiars, data_neixament, data_mort FROM mementomori ORDER BY id desc LIMIT 10';

  rowsdb = [];
  db.all(sql, [], (err, rows) => {
    if (err) {
      throw err;
    }
    rows.forEach((row) => {
      rowsdb.push(row);
      console.log(row.names);
    });
  });

  // console.log({db: rowsdb})

}


let sql = 'SELECT id, names, causa, familiars, data_neixament, data_mort FROM mementomori ORDER BY id desc LIMIT 10';

db.serialize(() => {

  db.each(sql, (err, row) => {
    if (err) {
      console.log(err);
    }
    console.log(row);
    rowsdb.push(row);
  })
  
});

// db.close((err) => {
//   if (err) {
//     console.error(err.message);
//   }
//   console.log('Close the database connection.');
// });
