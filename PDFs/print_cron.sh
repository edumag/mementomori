#!/bin/bash

DIR=`dirname $BASH_SOURCE`

cd "$DIR"

for c in `seq 19` ; do

  # Recoger pendientes.
  curl -H 'Content-Type: text/plain' http://localhost:5000/print

  files=`find -maxdepth 1 -type f -name '*.tex'`

  if [ "$files" == '*.tex' ] ; then
    logger "print_cron.sh without files" 
    exit
  fi

  for f in $files; do

    echo "- $f"
    logger "print_cron.sh $f" > /dev/null 2> /dev/null
    pdflatex -interaction=nonstopmode $f 
    lpr "`basename ${f} '.tex'`.pdf"
    mv "$f" OLD/
    mv "`basename ${f} '.tex'`.pdf" OLD/
    mv "`basename ${f} '.tex'`.aux" OLD/
    mv "`basename ${f} '.tex'`.log" OLD/

  done

  echo -e "\n[$c]/[10]" 
  sleep 5s

done 
